# Backend CRUD API REST

Bienvenid@ a el seguimiento paso a paso de un ejemplo de WS REST con NodeJS que proporciona API CRUD para gestionar una DB MongoDB.

## Comenzando 🚀

Mediante las siguientes instrucciones obtendremos una copia personal del proyecto para comprender su funcionamiento, poder probar y desarrollar.

Ver el apartado de ** Despliegue ** para conocer cómo desplegar el proyecto.

### Pre-requisitos 📋

Necesitamos una máquina(preferiblemente Linux y última version estable de distribución Ubuntu) con un mínimo de 2GHz de proccesador, 4GB de RAM Y 25GB de HD. 
Una vez tenemos esta máquina, necesitaremos las siguientes herramientas:
-Chrome
-Visual Studio Code
-Postman
Una vez tenemos en nuestro sistema estas herramientas, solamente nos falta el último requisito:
>	* Ganas de aprender *

### Instalación 🔧

Siguiendo el trabajo inicial, estos serían los procesos de instalación a desarrollar:

Para la instalación de los prerequisitos podemos utilizar los siguientes atajos directamente desde la terminal:
>$sudo snap install --classic code
>$sudo snap install postman

##### Instalación de NodeJS
Instalamos el gestor de paquetes:
>$sudo apt update
>$sudo apt install npm
Instalamos una utilidad de ayuda para instalar y mantener las versiones:
>$sudo npm clean -f
>$sudo npm i -g n
Instalamos la última versión estable de NodeJS:
>$sudo n stable

##### Instalación del gestor de repositorios
Instalamos y configuramos git con nuestros datos de acceso:
>$sudo apt install git 
>$git config --global user.name rsm106
>$git config --global user.email rsm106@gcloud.ua.es
>$git config --list
Tenemos dos opciones:
Si tenemos un código previo en nuestro repositorio del que queremos trabajar lo clonaríamos de la siguiente manera:
>$git clone https://rraulsanchez@bitbucket.org/rraulsanchez/ubuntusd.git api-rest
En el caso de no tener un código previo, comenzamos de cero y creamos un repositorio local:
>$git init
>$echo "#Backend CRUD API REST" > README.md

##### Instalación de Express
Para proporcionar una capa adicional a NodeJS instalaremos la biblioteca express de esta manera:
>$npm i -S express

##### Instalación de Nodemon
Como gestor de proyectos lo instalaremos de la siguiente manera:
>$npm i -DD nodemon

##### Instalación de Morgan
Para poder utilizar logs y saber que está pasando en cada momento teniendo un registro, instalamos nuestro motor de registro:
>$npm i -S morgan

##### Instalación de MongoDB
Necesitaremos una base de datos para nuestro ejemplo, podremos instalarlo así:
>$sudo apt update
>$sudo apt install -y mongodb
También necesitaremos instalar en nuestro proyecto la biblioteca mongodb para trabajar con la base de datos:
>$npm i -S mongodb
>$npm i -S mongojs

Una vez tenemos todo esto en marcha, podemos desplegar nuestro proyecto y empezar a darle forma.

Si deseasemos verificar información de nuestro sistema o obener datos generales o específicos podríamos seguir los siguientes pasos mediante el cliente mongo:
>$mongo
>show dbs
>use SD
>show collections
>db.familia.find()
En el ejemplo anterior, usamos una base de datos, de la cual encontramos todas las filas de una de sus colecciones.
Este proceso para obtener datos del sistema también lo haremos mediante un proyecto en Postman, donde será todo más intuitivo.

## Ejecutando las pruebas ⚙️

Iniciamos Postman y vamos creando, probando y guardando las diferentes rutas del API que ofrece nuestro servidor. Algunos ejemplos de estas pruebas serían los siguientes:
Petición para listar todas las colecciones o tablas de la DB
>	GET http://localhost:3000/api
Petición para listar todas las filas de una tabla o colección llamada "familia"
>	GET http://localhost:3000/api/familia
Petición para listar un elemento en concrceto de la tabla "familia"
>	GET http://localhost:3000/api/familia/${id}
Creación de una fila para la colección "familia"
>	POST http://localhost:3000/api/familia
Modificación de un elemento determinado(por ejemplo, el id) de la tabla "familia"
>	PUT http://localhost:3000/api/familia/${id}
Borrar un elemento determinado(por ejemplo, el id) de la tabla "familia"
>	DELETE http://localhost:3000/api/familia/${id}

Una vez tenemos este conjunto de peticiones funcionales, debemos exportar nuestra carpeta con las peticiones a lo que en nuestro caso se llamará "crud.postman_collection.json".
Para realizar pruebas con estas peticiones bastará con importar nuevamente dicho archivo en Postman y ejecutar cada uno de los ejemplos que tiene la colección.

### Analice las pruebas end-to-end 🔩
Las pruebas E2E (end-to-end) se utilizan para garantizar que el flujo de la aplicación funcione como se esperaría.
En nuestro caso, utilizaremos NodeJS para implementar los casos de prueba end-to-end.

### Y las pruebas de estilo de codificación ⌨️

_Explica qué verifican estas pruebas y por qué_

```
Proporciona un ejemplo
```

## Despliegue 📦

Tras haber seguido todos los pasos de instalación y creación de nuestro proyecto, debemos desplegar nuestra base de datos mongoDB en una terminal: 
>$sudo systemctl start mongodb
Así como nuestro servicio API REST a través de nodemon para gestionar cambios en otra terminal:
>$npm start
Si lo deseamos, también podríamos iniciar el cliente mongo para gestionar la base de datos en otra terminal: 
>$mongo --host localhost:27017

## Construido con 🛠️

Para realizar la práctica se han utilizado las siguientes herramientas:

* [NodeJS](http://nodejs.org) - El entorno de ejecución utilizado
* [MongoDB](https://mongodb.com) - Base de datos no estructurada

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)
* **Raúl Sánchez** - *Documentación* - [rsm106](https://bitbucket.org/rraulsanchez/)

## Licencia 📄

Este proyecto está bajo la Licencia (Raúl Sánchez Marqués) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Agradecer la guía facilitada por el profesor, al igual que su buena actitud.
 
